import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Check from '@material-ui/icons/Check';
import StepConnector from '@material-ui/core/StepConnector';
import Button from '@material-ui/core/Button';
import DeliveryAddress from './delivery-form/address-form';
import QuestionContact from './delivery-form/question-contact';
import ChangeAddressModal from './delivery-modal/change-address-modal';
import ChangeProvider from './delivery-form/change-provider';
import PaymentInformation from './delivery-form/payment-info';
import OverviewDetails from './delivery-form/overview-details';
import SepaDebit from './delivery-modal/sepa-detail';



const QontoConnector = withStyles({
  alternativeLabel: {
    bottom: 0,
    left: '0',
    right: '0',
    top:'inherit',
  },
  // active: {
  //   '& $line': {
  //     borderColor: 'green',
  //   },
  // },
  completed: {
    '& $line': {
      borderColor: '#E2001A',
    },
  },
  line: {
    borderColor: 'transparent',
    borderTopWidth: 3,
    borderRadius: 1,
  },
})(StepConnector);

const useQontoStepIconStyles = makeStyles({
  root: {
    color: '#000',
    display: 'flex',
    alignItems: 'center',
  },
  // active: {
  //   color: '#2b2b2b',
  // },
  // circle: {
  //   width: 8,
  //   height: 8,
  //   borderRadius: '50%',
  //   backgroundColor: 'currentColor',
  // },
  completed: {
    color: '#2b2b2b',
    zIndex: 1,
    fontSize: 32,
  },
});

function QontoStepIcon(props) {
  const classes = useQontoStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />}
    </div>
  );
}

QontoStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
};


 

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  // button: {
  //   marginRight: theme.spacing(1),
  // },
  // instructions: {
  //   marginTop: theme.spacing(1),
  //   marginBottom: theme.spacing(1),
  // },
}));

function getSteps() {
  return [{number:'01', name: 'Persönliche Angaben'} ,
  {number:'02', name: 'Kontaktdaten'},
  {number:'03', name: 'Angaben zum Anbieterwechsel'},
  {number:'04', name: 'Zahlungsinformationen'},
  {number:'05', name: 'Zusammenfassung'},
  ];
}


function getStepContent(step) {
  switch (step) {
    case 0:
      return <DeliveryAddress/>
    case 1:
      return <QuestionContact/>
    case 2:
      return <ChangeProvider/>
      case 3:
      return <PaymentInformation/>
    default:
      return <OverviewDetails/>;
  }
}


export default function CustomizedSteppers() {

//For modal
  const [open, setOpen] = React.useState(false);
  const [opendebit, setOpendebit] = React.useState(false);
  

  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    if (activeStep < steps.length - 1){
      //for modal
      if (activeStep == 0) {
        setOpen(true);
        return;
      }
      if (activeStep == 3) {
        setOpendebit(true);
        return;
      }
  //for modal
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    if (activeStep > 0){
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  }
  };

  const handleReset = () => {
    setActiveStep(0);
  };

//for dialog
  const handleClose = () =>{
    setOpen(false);
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }
  //for dialog step 4
  const handleCloseDebit = () =>{
    setOpendebit(false);
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }

  return (
 
<div className={classes.root}>

<div className="delivery-content">{getStepContent(activeStep)}
<div>

 <ChangeAddressModal open={open} handleClose={handleClose} /> 
 {/* addres modal call */} 

 <SepaDebit open={opendebit} handleCloseDebit={handleCloseDebit}/>

    <React.Fragment>
        {activeStep === steps.length ? (
          <div>
            <Button onClick={handleReset} className={classes.button}>
              Reset
            </Button>
          </div>
        ) : (
         
            <div className="step-btn d-flex justify-content-between">
              <Button variant="outlined" color="secondary" onClick={handleBack} className={classes.button}>
              Zurück
              </Button>
              <Button
              
                variant="contained"
                color="secondary"
                onClick={handleNext}
                className={classes.button}
              >
                {activeStep === steps.length - 1 ? 'Zahlungspflichtig abschließen' : 'Weiter'}
              </Button>
            </div>
         
        )}
      </React.Fragment>
      <div className="step-cover">

    <div  className={'step-first ' + (activeStep > 0? 'activelayer' : '')}></div>
      <Stepper alternativeLabel activeStep={activeStep} connector={<QontoConnector />}>
        {steps.map((label, i) => (
          <Step className={'step' + (i+1) + (i==activeStep? ' activestep' : '') } key={label}>
            <StepLabel StepIconComponent={QontoStepIcon}>
            {i>=activeStep? <span className="step-number">{label.number}</span> : ''}
            {i==activeStep? <span  className="step-name">{label.name}</span> : '' }
            </StepLabel>
          </Step>
        ))}
      </Stepper>
      <div className={'step-last ' + (activeStep == steps.length - 1? 'activelayer' : '')}></div>
      
      </div>
    </div>
    </div>
    </div>
  );
}
