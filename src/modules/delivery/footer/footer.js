import React from 'react';

function DeliveryFooter() {
  return (
      <footer>
        <div className="container">
            <div className="footer-content">
            <p>@SMA Solar Technology AG</p>
            <ul>
                <li><a href="#">Datenschulz</a></li>
                <li><a href="#">AGB</a></li>
                <li><a href="#">Impressum</a></li>
            </ul>
            </div>
        </div>
    </footer>
  );
}

export default DeliveryFooter;

