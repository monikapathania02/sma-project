import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Link } from '@material-ui/core';
import LoginModal from '../login-modal/login';



function DeliveryHeader() {

//for login modal
const [open, setOpen] = React.useState(false);

const toggle = () =>{
  setOpen(!open);
}



  return (
    
    <header>
      
      <h1>
        <img src={require('../../../assets/img/logo-sm.png')} />
      </h1>
        <div className="namecompany">
          <h2><strong>SMA</strong> Community Strom</h2>
          <small>Ein Angebot der Lumenaza</small>
        </div>
      <div>

      <LoginModal open={open} handleClose={toggle}/>
      <Button onClick={toggle}>
        Login 
        <img  src={require('../../../assets/img/login-icon.svg')}/>
      </Button>
         
       
        
      </div>

    </header>

  );
}

export default DeliveryHeader;
