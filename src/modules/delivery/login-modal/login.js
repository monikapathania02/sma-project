import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Link } from '@material-ui/core';


const useStyles = makeStyles({
  list: {
    width: '600px',
  },
  fullList: {
    width: 'auto',
  },
});

function LoginModal(
    props
) {

  const classes = useStyles();



  return (

            <Drawer  
            anchor='right'
            open={props.open}
            disableBackdropClick={true}
            >
              <div>
      <div className="login-form delivery-content">
        <div className="d-flex justify-content-end">
        <Button onClick={props.handleClose} ><i class="material-icons">
close
</i></Button>
</div>
        <h3>Login für SMA Kunden</h3>
        <small>Mit Sunny Portal oder Energy App Zugang Zeit sparen:</small>
        <form>
          <div className="col-12 pl-5 pr-5">
            <TextField className="fullwidth custom-input" required id="Benutzername" label="Benutzername" />
          </div>
          <div className="col-12 pl-5 pr-5">
            <TextField className="fullwidth custom-input" required id="Passwort" label="Passwort" />
          </div>
          <div className="col-12">
            <Link href="#">
              <i class="material-icons">keyboard_arrow_right</i> Passwort vergessen?
            </Link>
          </div>
          <div className="col-12 mt-5">
            <Button className="cust-btn" variant="contained" color="secondary">
              Login
            </Button>
          </div>
          <div className="col-12 orcode">
            <h6>Haben Sie eine 8-Stelligen Code bzw. QR Code zur Hand?</h6>
            <TextField className="fullwidth custom-input" required id="qrcode" placeholder="8-stelligen Code eingeben" />
            <small>oder</small>
            <div className="qrcode-scanner">
              QR Code scannen
          </div>
            <p>* Diese Daten sind notwendig für die Bestellung. Vielen Dank.</p>
          </div>
        </form>
      </div>
    </div>
            </Drawer>
        

  );
}

export default LoginModal;
