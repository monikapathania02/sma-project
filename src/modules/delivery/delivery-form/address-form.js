import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Link } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';

function DeliveryAddress() {
  const [anrede, setanrede] = React.useState('');


  //checkbox cahnge
  const [ischecked, setchecked] = React.useState(false);



  //DropDown
  const handleChange = (event) => {
    setanrede(event.target.value);
    setchecked((prev) => !prev);
  };

  return (
    <div className="container">

          <React.Fragment>
          <Slide direction="right" in={!ischecked} mountOnEnter unmountOnExit>
              <Paper elevation={4}>

            <h3>Ihre Lieferadresse</h3>
            <div className="col-lg-10 mx-auto">
              <form className="p-4 form-box" noValidate autoComplete="off">
                <div className="row">
                  <div className="col-md-4">

                    <FormControl required className="fullwidth">
                      <InputLabel id="demo-simple-select-label">Anreasfdsfdsfde</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={anrede}
                        onChange={handleChange}
                      >
                        <MenuItem value={10}>Verheiratet</MenuItem>
                        <MenuItem value={20}>unverheiratet</MenuItem>
                        <MenuItem value={30}>single</MenuItem>
                      </Select>
                    </FormControl>

                  </div>
                  <div className="col-md-4">
                    <TextField className="fullwidth custom-input" required id="Vorname" label="Vorname" />
                  </div>
                  <div className="col-md-4">
                    <TextField className="fullwidth custom-input" required id="Nachname" label="Nachname" />
                  </div>
                  <div className="col-md-6">
                    <TextField className="fullwidth custom-input" required id="Straße" label="Straße" />
                  </div>
                  <div className="col-md-3">
                    <TextField className="fullwidth custom-input" required id="Hausnr" label="Hausnr" />
                  </div>
                  <div className="col-md-3">
                    <TextField className="fullwidth custom-input" id="Zusatz" label="Zusatz" />
                  </div>
                  <div className="col-md-4">
                    <TextField className="fullwidth custom-input" required id="PLZ" label="PLZ" />
                  </div>
                  <div className="col-md-5">
                    <TextField className="fullwidth custom-input" required id="Musterort" label="Musterort" />
                  </div>

                  <div className="col-12">
                    <FormControlLabel className="custom-cehckbox"
                      control={
                        <Checkbox

                          name="checkedC"
                          disableRipple="true"
                          checkedIcon={<i class="material-icons">done</i>}
                          checked={ischecked}
                          onChange={() => setchecked(!ischecked)} />} label="Abweichende Rechnungsadresse" />
                  </div>
                </div>
              </form>

            </div>
            </Paper>
           </Slide>
          </React.Fragment>

          <React.Fragment>
            <Slide direction="right" in={ischecked} mountOnEnter unmountOnExit>
              <Paper elevation={4} >
                <h3>Ihre Rechnungsadresse</h3>
                <div className="col-lg-10 mx-auto">
                  <form className="p-4 form-box" noValidate autoComplete="off">
                    <div className="row">
                      <div className="col-md-4">

                        <FormControl required className="fullwidth">
                          <InputLabel id="demo-simple-select-label">Anrede</InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={anrede}
                            onChange={handleChange}
                          >
                            <MenuItem value={10}>Verheiratet</MenuItem>
                            <MenuItem value={20}>unverheiratet</MenuItem>
                            <MenuItem value={30}>single</MenuItem>
                          </Select>
                        </FormControl>

                      </div>
                      <div className="col-md-4">
                        <TextField className="fullwidth custom-input" required id="Vorname" label="Vorname" />
                      </div>
                      <div className="col-md-4">
                        <TextField className="fullwidth custom-input" required id="Nachname" label="Nachname" />
                      </div>
                      <div className="col-md-6">
                        <TextField className="fullwidth custom-input" required id="Straße" label="Straße" />
                      </div>
                      <div className="col-md-3">
                        <TextField className="fullwidth custom-input" required id="Hausnr" label="Hausnr" />
                      </div>
                      <div className="col-md-3">
                        <TextField className="fullwidth custom-input" id="Zusatz" label="Zusatz" />
                      </div>
                      <div className="col-md-4">
                        <TextField className="fullwidth custom-input" required id="PLZ" label="PLZ" />
                      </div>
                      <div className="col-md-5">
                        <TextField className="fullwidth custom-input" required id="Musterort" label="Musterort" />
                      </div>

                      <div className="col-12">
                        <FormControlLabel

                          className="custom-cehckbox"
                          control={
                            <Checkbox
                              name="checkedC"
                              disableRipple="true"
                              checkedIcon={<i class="material-icons">done</i>}
                              checked={ischecked} onChange={() => setchecked(!ischecked)} />}
                          label="Abweichende Rechnungsadresse" />
                      </div>
                    </div>
                  </form>
                </div>
              </Paper>
            </Slide>
          </React.Fragment>


      <div className="bottom-content col-lg-10 mx-auto">
        <p>* Diese Daten sind notwendig für die Bestellung. Vielen Dank.</p>


      </div>

    </div>



  );
}

export default DeliveryAddress;
