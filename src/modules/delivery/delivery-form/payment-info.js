import React from 'react';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
//tooltip
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { Portal } from '@material-ui/core';
const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: '#fff',
  },
  tooltip: {
    backgroundColor: '#fff',
    color: '#000',
    boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.26)',
    padding: '40px 50px',
    maxWidth: '550px',
  },
}));

function BootstrapTooltip(props) {

  const classes = useStylesBootstrap();

  return <Tooltip arrow classes={classes} {...props} />;
}

//tooltip

function PaymentInformation() {

  const [open, setOpen] = React.useState(false);

  const handleTooltipClose = () => {
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  return (
    <div className="container">
      <h3>Ihre Zahlungsinformationen</h3>
      <div className="col-lg-7 mx-auto">
        <form className="p-4 form-box" noValidate autoComplete="off">
          <div className="row">

            <div className="col-md-12">
              <TextField className="fullwidth custom-input" required id="Vorname" label="Vorname Kontoinhaber" />
            </div>
            <div className="col-md-12">
              <TextField className="fullwidth custom-input" required id="Nachname" label="Kontoinhaber" />

            </div>
            <div className="col-md-12">
              <TextField className="fullwidth custom-input" required id="Iban" label="IBAN" />

            </div>
            <div className="col-12 d-flex justify-content-start align-items-center input-icon">
              <div className="row">
                <div className="col-8">
                  <FormControlLabel 
                  className="custom-cehckbox" 
                  
                  control={<Checkbox disableRipple="true" name="checkedC" checkedIcon={<i class="material-icons">done</i>} />} 
                  label="Erteilung eines Manadats für das SEPA Basislastschriftverfahren" />
                </div>
                <div className="col-4 d-flex justify-content-start">



                  {open ? <Portal>
                    <div className="overlay-tooltip"></div>
                  </Portal> : ''}
                  <ClickAwayListener onClickAway={handleTooltipClose}>
                    <div>
                      <BootstrapTooltip

                        placement="top"
                        PopperProps={{
                          disablePortal: false
                        }}
                        onClose={handleTooltipClose}
                        open={open}
                        disableFocusListener
                        disableHoverListener
                        disableTouchListener
                        title={
                          <React.Fragment>
                            <div className="close-btn">
                              <Button onClick={() => { debugger; handleTooltipClose() }} >
                                <i class="material-icons">close</i>
                              </Button>
                            </div>

                            <Typography>Hinweis:</Typography>
                            <Typography>Ich kann innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen.
Es gelten dabei die mit meinem Kreditinstitut vereinbarten Bedingungen.</Typography>
                            <Typography>Vor dem ersten Einzug einer SEPA-Lastschrift wird mich die Lumenaza GmbH über den Einzug in dieser Verfahrensart unterrichten. </Typography>
                            <Typography>Zahlungsart: <br/>wiederkehrende Zahlungen</Typography>
                          </React.Fragment>
                        }>
                        <Button onClick={handleTooltipOpen}><i class="material-icons">info_outlined</i></Button>
                      </BootstrapTooltip>
                    </div>
                  </ClickAwayListener>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div className="bottom-content">
          <p>* Diese Daten sind notwendig für die Bestellung. Vielen Dank.</p>
        </div>

      </div>

    </div>
  );
}

export default PaymentInformation;