import React from 'react';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Link } from '@material-ui/core';

//Accordian
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';



//Accordian


function OverviewDetails() {

  //Accordian

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  //Accordian

  return (
    <div className="container">
      <h3>Ihre Angaben im Überblick</h3>
      <div className="col-lg-6 mx-auto">
        <div className="payment-detail">
          <p>
            Ihre Bestellung bei 3500 kWh:
        </p>
          <div className="payment-detail-box">
            <div className="detail-row">
              <div className="detail-name">
                Arbeitspreis
              </div>
              <div className="detail-amount">
                <p>25,61 CT</p>
               /
                <span>kWh</span>
              </div>
            </div>
            <div className="detail-row">
              <div className="detail-name">
                Grundpreis
              </div>
              <div className="detail-amount">
                <p>10,23 €</p>
                /
                <span>Monat</span>
              </div>
            </div>
            <div className="detail-row">
              <div className="detail-name">
                Mtl. Preis <small>(voraussichtlich)</small>
              </div>
              <div className="detail-amount">
                <p>84,99 €</p>
                /
                <span>Monat</span>
              </div>
            </div>
            <div className="detail-row blue-bg">
              <div className="detail-name">
                Einsparung
              </div>
              <div className="detail-amount">
                <p>10,24 €</p>
                /
                <span>Monat</span>
                <i class="material-icons pl-3">info_outlined</i>
              </div>
            </div>
          </div>
        </div>

        <div className="personal-product-details">
          <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
            <AccordionSummary
              expandIcon={<i class="material-icons">
              {expanded === 'panel1'? 'remove' : 'add'}
             </i>}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography>Geschätzer Verbrauch</Typography>
              <div className="d-flex justify-content-between w-100">
                <div className="prdt-left">Jahresgesamtverbrauch an Strom:</div>
                <div className="prdt-right">3500 kWh</div>
              </div>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Content coming soon...
          </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
            <AccordionSummary
              expandIcon={<i class="material-icons">
              {expanded === 'panel2'? 'remove' : 'add'}
             </i>}
              aria-controls="panel2bh-content"
              id="panel2bh-header"
            >
              <Typography>Persönliche Angaben</Typography>
              <div className="d-flex justify-content-between w-100">
                <div className="prdt-left">Anschrift:</div>
                <div className="prdt-right">Max & Erika, Mustermann <br />Musterstraße 123 <br />01234 Musterort</div>
              </div>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <Link className="d-flex align-items-center justify-content-end" href="#">
                  <i class="material-icons">keyboard_arrow_right</i> Bearbeiten
              </Link>
                <div className="d-flex justify-content-between w-100">
                  <div className="prdt-left">Kontaktdaten:</div>
                  <div className="prdt-right">
                    mustermann@gmx.de<br />keine Telefonnummer</div>
                </div>
                <Link className="d-flex align-items-center justify-content-end" href="#">
                  <i class="material-icons">keyboard_arrow_right</i> Bearbeiten
              </Link>

              </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
            <AccordionSummary
              expandIcon={<i class="material-icons">
              {expanded === 'panel3'? 'remove' : 'add'}
             </i>}
              aria-controls="panel3bh-content"
              id="panel3bh-header"
            >
              <Typography>Rechnungsadresse</Typography>
              <div className="d-flex justify-content-between w-100">
                <div className="prdt-left">Identisch mit Lieferadresse</div>
              </div>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                coming soon...
          </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
            <AccordionSummary
              expandIcon={<i class="material-icons">
              {expanded === 'panel4'? 'remove' : 'add'}
             </i>}
              aria-controls="panel4bh-content"
              id="panel4bh-header"
            >
              <Typography>Angaben zum Anbieterwechsel</Typography>
              <div className="d-flex justify-content-between w-100">
                <div className="prdt-left">Ihr Wechslegrund:</div>
                <div className="prdt-right">Stromanbieterwechsel</div>
              </div>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                coming soon
          </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion expanded={expanded === 'panel5'} onChange={handleChange('panel5')}>
            <AccordionSummary
              expandIcon={<i class="material-icons">
              {expanded === 'panel5'? 'remove' : 'add'}
             </i>}
              aria-controls="panel5bh-content"
              id="panel5bh-header"
            >
              <Typography>Zahlungsinformationen</Typography>
              <div className="d-flex justify-content-between w-100">
                <div className="prdt-left">IBAN:</div>
                <div className="prdt-right">DE41 0001 0051 0012 0002 12</div>
              </div>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                coming soon
          </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion expanded={expanded === 'panel6'} onChange={handleChange('panel6')}>
            <AccordionSummary
              expandIcon=
              {<i class="material-icons">
               {expanded === 'panel6'? 'remove' : 'add'}
              </i>}
              aria-controls="panel6bh-content"
              id="panel6bh-header"
            >
              <Typography>Passwort erstellen</Typography>

            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <h6 className="mb-4">Um die Produktbuchung abzuschließen vergeben Sie bitte noch ein Passwort:</h6>
                <form className="form-box" noValidate autoComplete="off">
                  <div className="row">

                    <div className="col-md-12">
                      <TextField className="fullwidth custom-input" required id="email" label="E-Mail Adresse" />
                    </div>
                    <div className="col-md-12">
                      <TextField className="fullwidth custom-input" required id="Passwort" label="Passwort" />

                    </div>
                    <div className="col-md-12">
                      <TextField className="fullwidth custom-input" required id="Pwiederholen" label="Passwort wiederholen" />

                    </div>
                    <div className="col-12">
                      <div className="pwd-captcha w-100">
                        captcha here
                      </div>
                    </div>
                    <div className="col-md-12">
                      <TextField className="fullwidth custom-input" required id="eintippen" label="Bitte Zeichen eintippen" />

                    </div>

                  </div>
                </form>
              </Typography>
            </AccordionDetails>
          </Accordion>
        </div>

        <div className="bottom-content">
          <p>* Diese Daten sind notwendig für die Bestellung. Vielen Dank.</p>
        </div>

        <div className="check-details row mt-5">
        <div className="col-12 d-flex justify-content-start align-items-center mb-2">
            <FormControlLabel className="custom-cehckbox" control={<Checkbox  disableRipple="true"
            checkedIcon={<i class="material-icons">done</i>} name="checkedC" />} 
            label={<label>Ja, ich stimme den <Link>AGBs</Link> der SMA Solar Technology AG zu*</label>} />
        </div>
         <div className="col-12 d-flex justify-content-start align-items-center mb-2">
            <FormControlLabel className="custom-cehckbox" control={<Checkbox  disableRipple="true"
            checkedIcon={<i class="material-icons">done</i>} name="checkedC" />} 
            label={<label>Ja, ich stimme den <Link>Datenschutz- und Datennutzeungsbestimmungen</Link> der SMA Solar Technology AG zu*</label>} />
        </div>
        <div className="col-12 d-flex justify-content-start align-items-center mb-2">
            <FormControlLabel className="custom-cehckbox" control={<Checkbox  disableRipple="true"
            checkedIcon={<i class="material-icons">done</i>} name="checkedC" />} 
            label={<label>Von dem mir zustehenden <Link>Wiederrufsrecht</Link> habe ich Kenntnis genommen*</label>} />
        </div>
        <div className="col-12 d-flex justify-content-start align-items-center mb-2">
            <FormControlLabel className="custom-cehckbox" control={<Checkbox  disableRipple="true"
            checkedIcon={<i class="material-icons">done</i>} name="checkedC" />} 
            label={<label>Ja, ich möchte Werbung erhalten, consectetuer adipiscing elit aenean commodo ligula eget dolor.</label>} />
        </div>


        </div>
        <div className="bottom-content">
          <p>*Sternchentext lorem ipsum sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
        </div>
        <div className="bottom-content">
        <p>Bei der Vermarktung des Stromprodukts kooperiert die SMA Solar Technology AG mit dem Stromversorgungsunternehmen Lumenaza GmbH. Stromkunden schließen ihren Vertrag über diese Plattform unmittelbar mit der Lumenaza GmbH ab. Die SMA Solar Technology AG stellt den Stromkunden diese Online-Plattform bereit. Die Lumenaza GmbH steht den Stromkunden als erster Ansprechpartner zur Verfügung.</p>
      </div>

      </div>

    </div>
  );
}

export default OverviewDetails;