import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

//tooltip
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { Portal } from '@material-ui/core';
const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: '#fff',
  },
  tooltip: {
    backgroundColor: '#fff',
    color: '#000',
    boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.26)',
    padding: '40px 50px',
    maxWidth: 'none',
  },
}));

function BootstrapTooltip(props) {

  const classes = useStylesBootstrap();

  return <Tooltip arrow classes={classes} {...props} />;
}

//tooltip


function ChangeProvider(props) {
  const classes = useStylesBootstrap();

  const [reason, setReason] = React.useState('');

  const [anrede1, setanrede1] = React.useState('');


  const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'))
  const [disbaledate, setDisbaledate] = React.useState(false);

  const handlecheckedDate = (event) => {

    if (
      event.target.checked == true
    ) {
      setDisbaledate(true);
      setSelectedDate(null);
    }
    else {
      setDisbaledate(false);
      setSelectedDate(new Date('2014-08-18T21:11:54'));

    }

  }

  const handleDateChange = (date) => {
    setSelectedDate(date);

  };
  //DropDown
  const handleChange = (event) => {
    setReason(event.target.value);

  };
  const handleChange1 = (event) => {
    setanrede1(event.target.value);

  };


  const [value, setValue] = React.useState('kündigen');

  const handleChange2 = (event) => {
    setValue(event.target.value);
  };

  const [open, setOpen] = React.useState(false);

  const handleTooltipClose = () => {
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  return (
    <div className="container">




      <h3>Ihre Gründe für einen Anbieterwechsel</h3>
      <div className="col-xl-10 col-lg-12 mx-auto change-provide">
        <form className="p-4 form-box step3-form" noValidate autoComplete="off">
          <div className="row">

            <div className="col-lg-6 d-flex justify-content-start align-items-center input-icon">
              <TextField className="fullwidth custom-input" required id="Stromverbrauch" label="Stromverbrauch letztes Jahr" />
              <i class="material-icons">info_outlined</i>
            </div>

            <div className="col-lg-6">
              <FormControl required className="fullwidth">
                <InputLabel id="demo-simple-select-label">Wechselgrund</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={reason}
                  onChange={handleChange}
                >
                  <MenuItem value={1}>Einzug / Umzug</MenuItem>
                  <MenuItem value={2}>Anbieterwechsel</MenuItem>
                  <MenuItem value={3}>single</MenuItem>
                </Select>
              </FormControl>
            </div>


            {reason == 2 || reason == 1 ?

              <React.Fragment>
                {reason == 2 ?
                  <React.Fragment>
                    <div className="col-lg-6">
                      <FormControl required className="fullwidth">
                        <InputLabel id="demo-simple-select-label1">Derzeitiger Versorger</InputLabel>
                        <Select
                          labelId="demo-simple-select-label1"
                          id="demo-simple-select"
                          value={anrede1}
                          onChange={handleChange1}
                        >
                          <MenuItem value={10}>Verheiratet</MenuItem>
                          <MenuItem value={20}>unverheiratet</MenuItem>
                          <MenuItem value={30}>single</MenuItem>
                        </Select>
                      </FormControl>
                    </div>
                    <div className="col-lg-6">
                      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange2}>
                        <FormControlLabel value="kündigen" control={<Radio />} label="Bitte für mich kündigen" />
                        <FormControlLabel value="gekündigt" control={<Radio />} label="Ich habe bereits gekündigt" />
                      </RadioGroup>

                    </div>
                    <div className="col-lg-6"></div>
                    <div className="col-lg-6">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          required
                          disableToolbar
                          variant="inline"
                          disabled={disbaledate}
                          format="MM/dd/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Lieferung zum"
                          value={selectedDate}
                          onChange={handleDateChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                      </MuiPickersUtilsProvider>

                      <FormControlLabel className="custom-cehckbox"
                        control={<Checkbox  disableRipple="true"
                        checkedIcon={<i class="material-icons">done</i>} name="checkedC"
                          onChange={(value) => { handlecheckedDate(value) }} />} label="Abweichende Rechnungsadresse" />
                    </div>
                  </React.Fragment>
                  : ''}




                <div className="col-lg-6 d-flex justify-content-start align-items-center input-icon">
                  <TextField className="fullwidth custom-input" required id="zählernummer" label="Zählernummer" />

                  {open ? <Portal>
                    <div className="overlay-tooltip"></div>
                  </Portal> : ''}
                  <ClickAwayListener onClickAway={handleTooltipClose}>
                    <div>
                      <BootstrapTooltip

                        placement="left"
                        PopperProps={{
                          disablePortal: false
                        }}
                        onClose={handleTooltipClose}
                        open={open}
                        disableFocusListener
                        disableHoverListener
                        disableTouchListener
                        title={
                          <React.Fragment>
                            <div className="close-btn">
                              <Button onClick={() => { debugger; handleTooltipClose() }} >
                                <i class="material-icons">close</i>
                              </Button>
                            </div>

                            <Typography className="toolhead">Sie finden DIESE je nach Zahlerlyp hier</Typography>
                            <div className="meter">
                              <div className="meter2">
                                <img className="img-fluid" src={require('../../../assets/img/meter-2.png')} />
                              </div>
                              <div className="meter1">
                                <img className="img-fluid" src={require('../../../assets/img/meter-1.png')} />
                              </div>
                            </div>
                          </React.Fragment>
                        }>
                        <Button onClick={handleTooltipOpen}><i class="material-icons">info_outlined</i></Button>
                      </BootstrapTooltip>
                    </div>
                  </ClickAwayListener>

                </div>

                {reason == 2 ? '' :
                  <div className="col-lg-6">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        required
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="Datum der Zählerinstallation"
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </div>
                }



                <div className="col-lg-6 d-flex justify-content-start align-items-center input-icon">
                  <TextField className="fullwidth custom-input" required id="Marktlokation" label="Marktlokation" />
                  <i class="material-icons">info_outlined</i>
                </div>


              </React.Fragment>
              : ''}
          </div>
        </form>
        <div className="bottom-content">
          <p>* Diese Daten sind notwendig für die Bestellung. Vielen Dank.</p>
        </div>

      </div>
    </div>

  );
}

export default ChangeProvider;
