import React from 'react';
import TextField from '@material-ui/core/TextField';



function QuestionContact() {
  return (
    <div className="container">
      <h3>Ihr Kontakt für Rückfragen</h3>
      <div className="col-lg-7 mx-auto">
        <form className="p-4 form-box" noValidate autoComplete="off">
          <div className="row">

            <div className="col-md-12">
              <TextField className="fullwidth custom-input" required id="email" label="E-Mail Adresse" />
            </div>
            <div className="col-md-12 d-flex justify-content-start align-items-center input-icon">
              <TextField className="fullwidth custom-input" required id="telefonnummer" label="Telefonnummer" />
              <i class="material-icons">info_outlined</i>
            </div>
          </div>
        </form>
        <div className="bottom-content">
          <p>* Diese Daten sind notwendig für die Bestellung. Vielen Dank.</p>
        </div>

      </div>
      <div className="col-md-8 mx-auto info-know">
        <i class="material-icons">info_outlined</i>
        <div>
          <p>SMA Solar Technology AG kann mir per E-Mail und SMS Informationen über eigene ähnliche Produkte und Dienstleistungen schicken.
            </p>
          <p>
            Ich kann dem jederzeit widersprechen, z.B. per E-Mail, Brief oder Telefon penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
        </div>
      </div>
    </div>
  );
}

export default QuestionContact;