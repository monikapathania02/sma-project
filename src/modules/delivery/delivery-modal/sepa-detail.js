import React from 'react';
//for dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
//for dialog


function SepaDebit(
  props
) {
 


  return (
    <React.Fragment>
    {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
      Open max-width dialog
    </Button> */}
    <Dialog
      fullWidth={true}
      maxWidth={'md'}
      open={props.open}
      disableBackdropClick={true}
      aria-labelledby="max-width-dialog-title"
      className="custom-modal change-addres-modal"
    >
      <DialogTitle id="max-width-dialog-title">Erteilung eines Mandats für das SEPA<br/> Basislastschriftverfahren</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <div className="sepa-details">
            <ul>
              <li>
                <label>Zahlungsempfänger:</label>
                <span>Lumenaza GmbH<br/> Kreuzbergstraße<br/> 30, <br/>10965 Berlin</span>
              </li>
              <li>
                <label>Gläubigeridentifikationsnum mer:</label>
                <span>DE25ZZZ00001340544</span>
              </li>
               <li>
                <label>Mandatsreferenznummer:</label>
                <span>Diese wird Ihnen seperat mitgeteilt</span>
              </li>
            </ul>
          </div>
          <p className="mb-3">Ich ermächtige die Lumenaza GmbH, Zahlungen von meinem Konto mittels Lastschrift einzuziehen. <br/>Zugleich weise ich mein Kreditinstitut an, die von der Lumenaza GmbH auf mein Konto gezogenen Lastschriften einzulösen.</p>
        </DialogContentText>
       
      </DialogContent>
      <DialogActions>
        <Button className="cust-btn" onClick={props.handleCloseDebit} variant="contained" color="secondary">
        Ja, erteilen
        </Button>
      </DialogActions>
    </Dialog>
  </React.Fragment>
  );
}
export default SepaDebit;