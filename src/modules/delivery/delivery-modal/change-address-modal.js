import React from 'react';

//for dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
//for dialog


function ChangeAddressModal(
    props
) {

//for dialog

//for dialog
  return (
 
      
      //for dialog
<React.Fragment>
      {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Open max-width dialog
      </Button> */}
      <Dialog
        fullWidth={true}
        maxWidth={'md'}
        open={props.open}
        disableBackdropClick={true}
        aria-labelledby="max-width-dialog-title"
        className="custom-modal change-addres-modal"
      >
        <DialogTitle id="max-width-dialog-title">Anderung der Adresse</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <img src={require('../../../assets/img/alert-modal.svg')}/>
            <br/>
            Die Adresse wird für Ihre neue Anschrift gültig...<br/> 
            Falls Sie Ihre Stammdaten ändern möchten gehen Sie bitte über Ihren Login über<br/> ennexOS (Sunny Portal) unter Einstellungen ...
          </DialogContentText>
         
        </DialogContent>
        <DialogActions>
          <Button className="cust-btn" onClick={props.handleClose} variant="contained" color="secondary">
          Anderung der Adresse
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>


    //for dialog


  );
}

export default ChangeAddressModal;
