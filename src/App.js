import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import CustomizedSteppers from './modules/delivery/step';
import DeliveryHeader from './modules/delivery/header/delivery-header';
import DeliveryFooter from './modules/delivery/footer/footer';

// import Header from './shared/header';
// import Product from './components/product/product';

function App() {
  return (
    <div className="App">
      <div className="delivery-wrap">
      {/* <Header /> */}
      {/* <Product /> */}
      <DeliveryHeader/>
      <CustomizedSteppers/>
     <DeliveryFooter/>
    </div>
    </div>
  );
}

export default App;
